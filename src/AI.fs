﻿namespace AI

open Model

module AIModel =
    let mutable internal AICOLOR = false
    let mutable gameover = false
    let mutable log  = false
    type PriceList =
        | Pawn = 10
        | Rook = 50
        | Knight = 25
        | Bishop = 25
        | King = 666
        | Queen = 100
        | empty = 0

module AIController =
    open AIModel

    let AllPossibleSteps ( dashboard : Dashboard, color : ColorType) =
        ((dashboard : Dashboard).Grid : array<array<CellDashboard>>)
        |> Seq.mapi /> fun y sx -> 
            sx  |> Seq.mapi /> fun x s -> 
                new SPosition(y,x) |> fun pos ->
                    if (not s.IsEmpty) && s.CellState.Color = color then 
                        let cs = s.CellState
                        let psteps = cs.motion dashboard pos color
                        if psteps.IsEmpty then
                            None
                        else
                            Some(psteps, cs, pos)
                    else None
            |> Seq.choose id
        |> Seq.concat

module AICore =
    open Model
    open AIController
    open AIModel

    open System

    let SetColor(color : String, underuse : bool) =
        match color with
        | c when c.ToUpper() = "WHITE" -> AICOLOR <- (if underuse then true else false); true
        | c when c.ToUpper() = "BLACK" -> AICOLOR <- (if underuse then false else true); true
        | _ -> false

    let AI(dashboard : Dashboard, verbose : bool, debug : bool) = 
        let aidirectStep(step : SPosition * SPosition) =
            let CE = ( dashboard.Cell(fst step) )
            dashboard.Grid.[(snd step).verticalPosition].[(snd step).horizontalPosition] <- CellDashboard(Some(CE.CellState))
            dashboard.Grid.[(fst step).verticalPosition].[(fst step).horizontalPosition].Clean()
        
        let Psteps = 
            AllPossibleSteps(dashboard, match(AICOLOR) with  | true -> ColorType.WHITE
                                                             | false -> ColorType.BLACK )

        if ( Seq.length Psteps ) > 0 then
            let Aanalize = 
                [for (steps,cell,pos) in Psteps do
                    for step in steps do
                        let _,_,f2 = step
                        if f2 then yield (step, cell, pos) ]

            let mutable iteration3 = "" // A bit OOP
            if (Seq.length Aanalize) = 0 then
                let element =
                    Psteps |> Seq.nth( System.Random().Next(Seq.length Psteps) ) // TODO: Random step ?
                           |> fun (steps,cell,pos) -> 
                                let (SP,C,_) = (steps |> Seq.nth( System.Random().Next(Seq.length steps) )) 
                                (SP),(C), cell, pos

                element |> fun (_to,_,_,_go) ->
                    aidirectStep(_go,_to)

                if verbose then
                    let t,s,c,p = element
                    iteration3 <- ( p.ToString() + " | " + c.ToString() + " -> " + t.ToString() + " | " + s.ToString() )
            else
                let element =
                    Aanalize 
                    |> Seq.map /> 
                        fun ((SP,C,_),cell,pos) -> 
                            match C.CellState with
                            | :? Model.King     -> PriceList.King
                            | :? Model.Queen    -> PriceList.Queen
                            | :? Model.Pawn     -> PriceList.Pawn
                            | :? Model.Rook     -> PriceList.Rook
                            | :? Model.Knight   -> PriceList.Knight
                            | :? Model.Bishop   -> PriceList.Bishop
                            | _                 -> PriceList.empty
                            , SP, C, cell, pos
                    |> Seq.maxBy /> fun (P,SP,C,cell,pos) -> P 

                gameover <-
                    element |> fun (P,_to,_,_,_go) ->
                        aidirectStep(_go,_to)
                        P.Equals PriceList.King

                if verbose then
                    let _,t,s,c,p = element
                    iteration3 <- 
                        ( p.ToString() + " | " + c.ToString() + " -> " + t.ToString() + " | " + s.ToString() )
                        + if gameover then (Environment.NewLine + "!!! CHECKMATE !!!" + Environment.NewLine) else ""

            if verbose then
                let iteration1 =
                    if (Seq.length Psteps) = 0 then "No possible steps !"
                    else
                        if debug then
                            String.Join (Environment.NewLine,
                                [for steps,cell,pos in Psteps do 
                                    for (SP,C,_) in steps -> 
                                        pos.ToString() + " | " + cell.ToString() + " -> " + SP.ToString() + " | " + C.ToString() ])
                        else null

                let iteration2 = 
                    if debug then
                        if (Seq.length Aanalize) = 0 then "Nothing to attack, Generating random step :D"
                        else "Generating attack !"
                    else null

                if debug then
                    String.Join (Environment.NewLine,
                        [iteration1; iteration2; iteration3])
                else iteration3
            else null
        else null