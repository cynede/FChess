﻿namespace FChess

module cprintf =
    let cprintf c fmt = 
        Printf.kprintf 
            (fun s -> 
                let old = System.Console.ForegroundColor 
                try 
                  System.Console.ForegroundColor <- c
                  System.Console.Write s
                finally
                  System.Console.ForegroundColor <- old) 
            fmt
    let cprintfn c fmt = 
        cprintf c fmt
        printfn ""

module Console =
    open cprintf
    open Model
    open System
    open AI

    let mutable username = "user"
    let mutable debug = false
    let mutable game = false

    let (<?>) action =
        match AIModel.gameover with
        | true -> cprintfn ConsoleColor.Red "Game is over, if you want to play again type newgame"
        | false -> action()

    let help() =
        cprintfn ConsoleColor.DarkYellow @"Commands description : 
        play X :
            Play the game against AI :)
            Example : play white (Use stop to stop game)
        debug: 
            Enable / Disable debug (verbose) output
        log: 
            Enable / Disable log (game logging)
	    grep X :
	        Check current cell state
	        Example : grep a5
	    rm X 
	        Clean current cell state
	        Example : rm a5
	    ls :
	        Draws whole (current) dashboard
	    cp X Y :
	        Making a step
	        Example : cp a2 a1
	    ai Color :
	        Playing with TeAI ! (AI STEP)
	        Example : ai white
	    newgame :
	        Creating new game
	    exit
	        Exit from test application"

    let user() = cprintf ConsoleColor.DarkGreen  "%s@FChess# " username

    let grep(position : string, dashboard : Dashboard) =
        let pos = SPosition.Parse(position)
        if pos.IsSome then
            cprintf ConsoleColor.DarkGray "Cell : %s" <| dashboard.Cell(pos.Value).ToString()
            printfn ""
        else cprintfn ConsoleColor.Red "Wrong dashboard position format"

    let rm(position : string, dashboard : Dashboard) =
        let pos = SPosition.Parse(position)
        if pos.IsSome then
            dashboard.Remove(pos.Value)
            cprintf ConsoleColor.DarkGray "Cell : %s" <| dashboard.Cell(pos.Value).ToString()
            printfn ""
        else cprintfn ConsoleColor.Red "Wrong dashboard position format"

    let ls ( dashboard : Dashboard ) = 
        dashboard.ToList()
        |> Seq.iter /> fun line ->
                line 
                |> Seq.iter /> fun (s,fcolor,b2) -> 
                    if b2 then
                        printf "|"; cprintf (if fcolor then // Black
                                                ConsoleColor.DarkCyan 
                                             else           // White
                                                ConsoleColor.DarkYellow) "%s" s
                    else
                        printf "|"; cprintf ConsoleColor.DarkMagenta "%s" s
                printfn "|"

    let ai(color : string, dashboard : Dashboard) =
            if AICore.SetColor(color,true) then
                try
                    cprintf ConsoleColor.DarkGray "%s" <| AICore.AI(dashboard, true, debug); printfn ""
                    ls dashboard
                with
                    | e -> cprintf ConsoleColor.Red "%s" e.Message; printfn ""
            else cprintf ConsoleColor.Red "Wrong color name : %s , use black or white instead." color; printfn ""

    let cp(position1 : string, position2 : string, dashboard : Dashboard) =
        let pos1 = SPosition.Parse(position1)
        let pos2 = SPosition.Parse(position2)
        if pos1.IsSome && pos2.IsSome then
            let mutable msg = ""
            if dashboard.TryStep(pos1.Value,pos2.Value, ref msg) then 
                cprintf ConsoleColor.DarkGray "%s" msg; printfn ""
                ls dashboard
                if game then
                    try
                        cprintf ConsoleColor.DarkGray "%s" <| AICore.AI(dashboard, true, debug); printfn ""
                        ls dashboard
                    with
                        | e -> cprintf ConsoleColor.Red "%s" e.Message; printfn ""
            else cprintf ConsoleColor.Red "%s" msg; printfn ""
        else cprintfn ConsoleColor.Red "Wrong dashboard position format"

    let play(color : string, dashboard : Dashboard) =
        if AICore.SetColor(color,false) then
            game <- true
        else cprintf ConsoleColor.Red "Wrong color name : %s , use black or white instead." color; printfn ""

    let newgame ( dashboard : Dashboard ref) = 
        cprintfn ConsoleColor.DarkCyan "Creating new game State"
        AIModel.gameover <- false
        dashboard := new Dashboard()
        cprintfn ConsoleColor.DarkCyan "Done"

    let stop () = game <- false

    let console(dashboard : Dashboard ref) = 
        let rec eat (command : string) =
            let prelude() = user(); eat( Console.ReadLine() )
            match String.IsNullOrEmpty command with
            | true -> prelude()
            | false ->
                command.Split(' ','(',')') 
                |> Seq.filter(fun s -> s.Length <> 0)
                |> fun C ->
                    (Seq.head C).ToUpper() |> fun head ->
                        let checkparams needparams triller = 
                            if ( Seq.length C ) > needparams then triller() else 
                            cprintfn ConsoleColor.Red "Not enouth parameters for this command"
                        let ifparams needparams triller1 triller2 = 
                            if ( Seq.length C ) > needparams then triller1() 
                            else triller2()
                        if head = "EXIT" then printfn "Bye bye"; System.Threading.Thread.Sleep 10
                        else
                            match head with 
                            | "DEBUG"   -> debug        <- (debug = false); cprintfn ConsoleColor.DarkMagenta       <| if debug then "Debug Enabled" else "Debug Disabled";
                            | "LOG"     -> AIModel.log  <- (AIModel.log = false); cprintfn ConsoleColor.DarkMagenta <| if AIModel.log then "Log Enabled" else "Log Disabled";
                            | "STOP"    -> stop()
                            | "HELP"    -> help()
                            | "NEWGAME" -> newgame( dashboard )
                            | "LS"      -> ls( !dashboard )
                            | "GREP"    -> checkparams 1  <|fun()-> grep( (Seq.nth(1) C), !dashboard )
                            | "PLAY"    -> checkparams 1  <|fun()-> (<?>) <|fun()-> play( (Seq.nth(1) C), !dashboard )
                            | "CP"      -> checkparams 2  <|fun()-> (<?>) <|fun()-> cp( (Seq.nth(1) C), (Seq.nth(2) C), !dashboard )
                            | "RM"      -> checkparams 1  <|fun()-> (<?>) <|fun()-> rm( (Seq.nth(1) C), !dashboard )
                            | "AI"      -> ifparams 1     <|fun()-> (<?>) <|fun()-> ai( (Seq.nth(1) C), !dashboard )
                                                          <|fun()-> 
                                                                ai((match AI.AIModel.AICOLOR with
                                                                    | true -> "WHITE"
                                                                    | false -> "BLACK"), !dashboard)
                                                                AI.AIModel.AICOLOR <- not AI.AIModel.AICOLOR
                            | _ -> ()
                            prelude()
        user()
        eat( Console.ReadLine() )

module Main =
    open Console
    open Model
    console( ref <| new Dashboard() )
