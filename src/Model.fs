﻿module Model

open System
open System.Linq
open System.Collections
open System.Collections.Generic
open System.Collections.ObjectModel

open System.Runtime.InteropServices 

let (/>) f a = f a

type public Pawn =
    inherit Figure
    override X.Name
        with get() = " Pawn "
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []
        match cl with
            | ColorType.WHITE ->
                if ( ps.verticalPosition > 0 ) then
                    let canAttack(sp : SPosition) =
                        let testCell : CellDashboard = db.Cell(sp)
                        if (not testCell.IsEmpty) then
                            if ((testCell.CellState : Figure).Color <> cl) then
                                outA := (sp, testCell, true) :: !outA
                    if ( ps.horizontalPosition < 7 ) then
                        canAttack ( SPosition(ps.verticalPosition - 1, ps.horizontalPosition + 1 ) )
                    if ( ps.horizontalPosition > 0 ) then
                        canAttack ( SPosition(ps.verticalPosition - 1, ps.horizontalPosition - 1 ) ) 
                    if ps.verticalPosition = 6 then 
                        let _testposition = SPosition(ps.verticalPosition - 2, ps.horizontalPosition )
                        let _testCell : CellDashboard = db.Cell(_testposition)
                        if (_testCell.IsEmpty) then
                            outA := (_testposition, _testCell, false) :: !outA
                    let testposition = SPosition(ps.verticalPosition - 1, ps.horizontalPosition )
                    let testCell : CellDashboard = db.Cell(testposition)
                    if (testCell.IsEmpty) then
                        outA := (testposition, testCell, false) :: !outA
            | ColorType.BLACK -> 
                if ( ps.verticalPosition < 7 ) then
                    let canAttack(sp : SPosition) =
                        let testCell : CellDashboard = db.Cell(sp)
                        if (not testCell.IsEmpty) then
                            if ((testCell.CellState : Figure).Color <> cl) then
                                outA := ((sp, testCell, true) :: !outA)
                    if ( ps.horizontalPosition < 7 ) then
                        canAttack ( SPosition(ps.verticalPosition + 1, ps.horizontalPosition + 1 ) )
                    if ( ps.horizontalPosition > 0 ) then
                        canAttack ( SPosition(ps.verticalPosition + 1, ps.horizontalPosition - 1 ) ) 
                    if ps.verticalPosition = 1 then 
                        let _testposition = SPosition(ps.verticalPosition + 2, ps.horizontalPosition )
                        let _testCell : CellDashboard = db.Cell(_testposition)
                        if (_testCell.IsEmpty) then
                            outA := (_testposition, _testCell, false) :: !outA
                    let testposition = SPosition(ps.verticalPosition + 1, ps.horizontalPosition )
                    let testCell : CellDashboard = db.Cell(testposition)
                    if (testCell.IsEmpty) then
                        outA := (testposition, testCell, false) :: !outA
        !outA
    new() = { }
and Rook =
    inherit Figure
    override X.Name
        with get() = " Rook "
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []  
        if( ps.verticalPosition >= 0 ) then
            let checkgonext(sp : SPosition) =
                let testCell : CellDashboard = db.Cell(sp)
                if (testCell.IsEmpty) then
                    outA := (sp, testCell, false) :: !outA; true
                else
                    if ((testCell.CellState : Figure).Color = ( match(cl) with
                                                                | ColorType.WHITE -> ColorType.BLACK
                                                                | ColorType.BLACK -> ColorType.WHITE
                                                                ) ) then
                        outA := (sp, testCell, true) :: !outA
                    false
            let check(cp : SPosition) =
                let cangoleft   = cp.horizontalPosition > 0
                let cangoright  = cp.horizontalPosition < 7
                let cangodown   = cp.verticalPosition   > 0
                let cangoup     = cp.verticalPosition   < 7
                [| cangoleft; cangoright; cangodown; cangoup |]
            let rec gol(i : int) =
                let next = new SPosition( ps.verticalPosition, ps.horizontalPosition - i )
                let checknext = check (next)
                if (checknext.[0]) then
                    if ( checkgonext( next ) ) then
                        gol(i + 1)
            let rec god(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition )
                let checknext = check (next)
                if (checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        god(i + 1)    
            let rec gor(i : int) =
                let next = new SPosition( ps.verticalPosition, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1]) then
                    if ( checkgonext( next ) ) then
                        gor(i + 1)  
            let rec gou(i : int) =
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition )
                let checknext = check (next)
                if (checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        gou(i + 1)
            gol(1); gor(1); god(1); gou(1);
        !outA 
    new() = { }
and Queen =
    inherit Figure
    override X.Name
        with get() = "Queen "
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []  
        if( ps.verticalPosition >= 0 ) then
            let checkgonext(sp : SPosition) =
                let testCell : CellDashboard = db.Cell(sp)
                if (testCell.IsEmpty) then
                    outA := (sp, testCell, false) :: !outA; true
                else
                    if ((testCell.CellState : Figure).Color = ( match(cl) with
                                                                | ColorType.WHITE -> ColorType.BLACK
                                                                | ColorType.BLACK -> ColorType.WHITE
                                                                ) ) then
                        outA := (sp, testCell , true) :: !outA
                    false   
            let check(cp : SPosition) =
                let cangoleft   = cp.horizontalPosition > 0
                let cangoright  = cp.horizontalPosition < 7
                let cangodown   = cp.verticalPosition   > 0
                let cangoup     = cp.verticalPosition   < 7
                [| cangoleft; cangoright; cangodown; cangoup |]
            let rec gold(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition - i )
                let checknext = check (next)
                if (checknext.[0] && checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        gold(i+1)
            let rec gol(i : int) =
                let next = new SPosition( ps.verticalPosition, ps.horizontalPosition - i  )
                let checknext = check (next)
                if (checknext.[0]) then
                    if ( checkgonext( next ) ) then
                        gol(i+1)
            let rec god(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition )
                let checknext = check (next)
                if (checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        gol(i+1) 
            let rec goru(i : int) =
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1] && checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        goru(i+1)              
            let rec gor(i : int) =
                let next = new SPosition( ps.verticalPosition, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1]) then
                    if ( checkgonext( next ) ) then
                        gor(i+1)
            let rec gou(i : int) = 
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition )
                let checknext = check (next)
                if (checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        gou(i+1)
            let rec gord(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1] && checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        gord(i+1)          
            let rec golu(i : int) = 
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition - i )
                let checknext = check (next)
                if (checknext.[0] && checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        golu(i+1)
            golu(1); gold(1); gord(1); goru(1); gol(1); gor(1); god(1); gou(1);
        !outA 
    new() = { }
and Knight =
    inherit Figure
    override X.Name
        with get() = "Knight"
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []  
        let ddh = (ps.horizontalPosition - 2)
        let uuh = (ps.horizontalPosition + 2)
        let uuv = (ps.verticalPosition + 2)
        let ddv = (ps.verticalPosition - 2)  
        let analize(x : int, y : int) =
            let sp = new SPosition(y,x)
            let testCell : CellDashboard = db.Cell(sp)
            if (testCell.IsEmpty) then
                outA := (sp, testCell ,false) :: !outA
            else
                if ((testCell.CellState : Figure).Color <> cl) then
                    outA := (sp, testCell ,true) :: !outA
        let dv =  (ps.verticalPosition - 1)
        if ( dv >= 0 ) then
            if ( uuh < 8 ) then
                analize (uuh,dv)
            if ( ddh >= 0 ) then
                analize (ddh,dv)
        let dh = (ps.horizontalPosition - 1)
        if ( dh >= 0 ) then
            if ( uuv < 8 ) then
                analize (dh,uuv)
            if ( ddv >= 0 ) then
                analize (dh,ddv)
        let uv = (ps.verticalPosition + 1)
        if ( uv < 8 ) then
            if ( uuh < 8 ) then
                analize (uuh,uv)
            if ( ddh >= 0 ) then
                analize (ddh,uv)
        let uh = (ps.horizontalPosition + 1)
        if ( uh < 8 ) then
            if ( uuv < 8 ) then
                analize (uh,uuv)
            if ( ddv >= 0 ) then
                analize (uh,ddv)  
        !outA
    new() = { }
and King =
    inherit Figure
    override X.Name
        with get() = " King "
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []  
        if( ps.verticalPosition >= 0 ) then
            let check(sp : SPosition) =
                let testCell : CellDashboard = db.Cell(sp)
                if (testCell.IsEmpty) then 
                    outA := (sp, testCell, false) :: !outA
                else
                    if ((testCell.CellState : Figure).Color = ( match(cl) with
                                                                | ColorType.WHITE -> ColorType.BLACK
                                                                | ColorType.BLACK -> ColorType.WHITE
                                                                ) ) then
                        outA := (sp, testCell, true) :: !outA
            let cangoleft   = ps.horizontalPosition > 0
            let cangoright  = ps.horizontalPosition < 7
            let cangodown   = ps.verticalPosition   > 0
            let cangoup     = ps.verticalPosition   < 7                
            if (cangoleft && cangodown)     then
                check( SPosition(ps.verticalPosition - 1, ps.horizontalPosition - 1 ) )    
            if (cangoleft)                  then
                check( SPosition(ps.verticalPosition, ps.horizontalPosition - 1     ) )     
            if (cangodown)                  then
                check( SPosition(ps.verticalPosition - 1, ps.horizontalPosition     ) )      
            if (cangoup)                    then
                check( SPosition(ps.verticalPosition + 1, ps.horizontalPosition     ) )     
            if (cangoright)                 then
                check( SPosition(ps.verticalPosition, ps.horizontalPosition + 1     ) )    
            if (cangoright && cangoup)      then
                check( SPosition(ps.verticalPosition + 1, ps.horizontalPosition + 1 ) )    
            if (cangoleft && cangoup)       then
                check( SPosition(ps.verticalPosition + 1, ps.horizontalPosition - 1 ) )  
            if (cangoright && cangodown)    then
                check( SPosition(ps.verticalPosition - 1, ps.horizontalPosition + 1 ) )
        !outA 
    new() = { }
and Bishop =
    inherit Figure
    override X.Name
        with get() = "Bishop"
    override X.motion db ps cl =
        let outA : list<SPosition * CellDashboard * bool> ref = ref []  
        if ( ps.verticalPosition >= 0 ) then
            let checkgonext(sp : SPosition) =
                let testCell : CellDashboard = db.Cell(sp)
                if (testCell.IsEmpty)  then
                    outA := (sp, testCell, false) :: !outA; true
                else
                    if ((testCell.CellState : Figure).Color = ( match(cl) with
                                                                | ColorType.WHITE -> ColorType.BLACK
                                                                | ColorType.BLACK -> ColorType.WHITE
                                                                ) ) then
                        outA := (sp, testCell, true) :: !outA
                    false
            let check(cp : SPosition) =
                let cangoleft   = cp.horizontalPosition > 0
                let cangoright  = cp.horizontalPosition < 7
                let cangodown   = cp.verticalPosition   > 0
                let cangoup     = cp.verticalPosition   < 7
                [| cangoleft; cangoright; cangodown; cangoup |]       
            let rec gold(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition - i )
                let checknext = check (next)
                if (checknext.[0] && checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        gold(i+1)
            let rec goru(i : int) = 
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1] && checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        goru(i+1)
            let rec gord(i : int) =
                let next = new SPosition( ps.verticalPosition - i, ps.horizontalPosition + i )
                let checknext = check (next)
                if (checknext.[1] && checknext.[2]) then
                    if ( checkgonext( next ) ) then
                        gord(i+1)
            let rec golu(i : int) =
                let next = new SPosition( ps.verticalPosition + i, ps.horizontalPosition - i )
                let checknext = check (next)
                if (checknext.[0] && checknext.[3]) then
                    if ( checkgonext( next ) ) then
                        golu(i+1)
            golu(1); gold(1); gord(1); goru(1);
        !outA 
    new() = { }
and ColorType =
    | BLACK 
    | WHITE
    override C.ToString() =
        match C with
        | BLACK -> "BLACK"
        | WHITE -> "WHITE"
and SPosition(vp : int, hp : int) = 
    let mutable _verticalPosition   : int    = 0
    let mutable _horizontalPosition : int    = 0
    do
        _verticalPosition    <- vp
        _horizontalPosition  <- hp
    new() = new SPosition(0, 0)
    member SP.verticalPosition 
        with get() = _verticalPosition
    member SP.horizontalPosition   
        with get() = _horizontalPosition
    override SP.ToString() =
        let x = [| 'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H' |].[_horizontalPosition]
        String.Concat( x, (_verticalPosition + 1).ToString() )
    static member Parse(chessNotation : String) =
        if (chessNotation.ToUpper().[0] >= 'A' && chessNotation.ToUpper().[0] <= 'H' && chessNotation.[1] >= '1' && chessNotation.[1] <= '8')  then
            Some(
                new SPosition(
                    Int32.Parse(chessNotation.[1].ToString()) - 1
                    ,( Array.IndexOf([| 'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H' |], chessNotation.ToUpper().[0] ) )
                    )
                )
        else None
and [<AbstractClass>] Figure() =
    let mutable color : ColorType = WHITE   
    abstract member Name : string    
    member F.Color
        with get()  = color
        and set v   = color <- v
    abstract member motion : Dashboard -> SPosition -> ColorType -> list<SPosition * CellDashboard * bool>;
    override F.ToString() = F.Name
and CellDashboard(addingFigure : option<Figure>) =
    let mutable attacked    : bool      = false;
    let mutable cleaned     : bool      = true;
    let mutable _CellState  : option<Figure> = addingFigure;

    do cleaned <- if addingFigure.IsSome then false else true

    new() = new CellDashboard(None)
    member C.CellState
        with get() = _CellState.Value
    member C.IsEmpty
        with get() =  cleaned    
    member C.ToStringX() : string =
        if (cleaned) || _CellState.IsNone then 
            "      " 
        else 
            _CellState.Value.ToString()
    override C.ToString() : string =
        if (cleaned) then "Cleaned" else 
            if _CellState.IsSome then
                (" Figure : " + _CellState.Value.ToString() + " Color : " + _CellState.Value.Color.ToString())
            else "Missing"
    member C.Clean() =
        cleaned     <- true;
        _CellState  <- None;
and Dashboard() = 
    let mutable _grid : array<array<CellDashboard>> = [||]
    let DirectStep(start : CellDashboard, startPosition : String, targetPosition : SPosition) =
        _grid.[targetPosition.verticalPosition].[targetPosition.horizontalPosition] <- CellDashboard(Some(start.CellState))
        let startfigure = start.ToString()
        start.Clean()
        @"Figure " + startfigure + " moved to " + targetPosition.ToString() + " | " + startPosition + " is Empty"
    do
        let cellfigure(addingFigure : Figure, color : ColorType) : CellDashboard =
            addingFigure.Color <- color;
            new CellDashboard(Some(addingFigure))
        _grid <- [|[| cellfigure( new Rook(),      ColorType.BLACK);
                      cellfigure( new Knight(),    ColorType.BLACK);
                      cellfigure( new Bishop(),    ColorType.BLACK);
                      cellfigure( new Queen(),     ColorType.BLACK);
                      cellfigure( new King(),      ColorType.BLACK);
                      cellfigure( new Bishop(),    ColorType.BLACK);
                      cellfigure( new Knight(),    ColorType.BLACK);
                      cellfigure( new Rook(),      ColorType.BLACK)|];
                   [| cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK);
                      cellfigure( new Pawn(),      ColorType.BLACK) |];
                   [| new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard(); |];
                   [| new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard(); |];
                   [| new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard(); |];
                   [| new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard();
                      new CellDashboard(); new CellDashboard(); |];
                   [| cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE);
                      cellfigure( new Pawn(),      ColorType.WHITE) |];
                   [| cellfigure( new Rook(),      ColorType.WHITE);
                      cellfigure( new Knight(),    ColorType.WHITE);
                      cellfigure( new Bishop(),    ColorType.WHITE);
                      cellfigure( new Queen(),     ColorType.WHITE);
                      cellfigure( new King(),      ColorType.WHITE);
                      cellfigure( new Bishop(),    ColorType.WHITE);
                      cellfigure( new Knight(),    ColorType.WHITE);
                      cellfigure( new Rook(),      ColorType.WHITE)|];
            |]
    member D.Grid 
        with get()  = _grid
        and set v   = _grid <- v
    member D.Cell(position : SPosition) =
        (_grid.[position.verticalPosition].[position.horizontalPosition])
    member D.ToList() : list<list<string * bool * bool>> =
        let mutable outarray : list<list<string * bool * bool>> = []
        for temp in [0..7] do
            let i = 7 - temp
            let mutable outline : list<string * bool * bool> = []
            for cell in _grid.[i] do
                if (cell.IsEmpty) then
                    outline <- (cell.ToStringX(), false, false) :: outline;
                else
                    outline <- (cell.ToStringX(), (if (cell.CellState.Color=ColorType.WHITE) then true else false), true) :: outline;
            outarray <- outline :: outarray;
        outarray
    member D.TryStep(startPosition : SPosition, targetPosition : SPosition, [<Out>] message : byref<string>) : bool =
        let start = D.Cell(startPosition)
        if (start.IsEmpty) then
            message <- "there is no figure on start position"
            false
        else
            try
                message <- DirectStep(start, startPosition.ToString(), targetPosition)
                true
            with
            | exc ->
                message <- exc.Message
                false
    member D.Remove(position : SPosition) =
        _grid.[position.verticalPosition].[position.horizontalPosition] <- CellDashboard();
    member D.clone() : Dashboard =
        let cloned = new Dashboard();
        cloned.Grid <- _grid
        cloned
