Licensed under [GPLv2]
======================

``` fsharp
let console(dashboard : Dashboard ref) = 
	let rec eat (command : string) =
		command.Split(' ','(',')') 
		|> Seq.filter(fun s -> s.Length <> 0)
		|> fun C ->
			(Seq.head C).ToUpper() |> fun head ->
				let checkparams needparams triller = 
					if ( Seq.length C ) > needparams then triller() else 
					cprintfn ConsoleColor.Red "Not enouth parameters for this command"
				let ifparams needparams triller1 triller2 = 
					if ( Seq.length C ) > needparams then triller1() 
					else triller2()
				if head = "EXIT" then printfn "Bye bye"; w8
				else
					match head with 
					| "STOP"    -> stop()
					| "DEBUG"   -> debug <- (debug = false); cprintfn ConsoleColor.DarkMagenta <| if debug then "Debug Enabled" else "Debug Disabled";
					| "HELP"    -> help()
					| "NEWGAME" -> newgame( dashboard )
					| "LS"      -> ls( !dashboard )
					| "PLAY"    -> checkparams 1    <| fun () -> play( (Seq.nth(1) C), !dashboard )
					| "CP"      -> checkparams 2    <| fun () -> cp( (Seq.nth(1) C), (Seq.nth(2) C), !dashboard )
					| "GREP"    -> checkparams 1    <| fun () -> grep( (Seq.nth(1) C), !dashboard )
					| "RM"      -> checkparams 1    <| fun () -> rm( (Seq.nth(1) C), !dashboard )
					| "AI"      -> ifparams 1       <| fun () -> ai( (Seq.nth(1) C), !dashboard )
													<| fun () -> 
														ai((match AI.AIModel.AICOLOR with
															| true -> "WHITE"
															| false -> "BLACK"), !dashboard)
														AI.AIModel.AICOLOR <- not AI.AIModel.AICOLOR
					| _ -> ()
					user(); eat( Console.ReadLine() )
	user()
	eat( Console.ReadLine() )
```